package m4;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * 路由模式
 *      消息消费者 声明 direct交换机, 队列名. 将其绑定 并指定 关键词
 *
 */
public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        // 1.随机队列 2.交换机 3.使用绑定键 绑定
        // c.queueDeclare(queue, false,true,true,null);

        // 还可以由服务器来自动随机进行队列名的 命名, 非持久,独占,自动删除
        String queue = c.queueDeclare().getQueue();
        c.exchangeDeclare("direct_logs", BuiltinExchangeType.DIRECT);

        System.out.println("输入绑定键，用空格隔开："); // "aa   bb   cc"
        String s = new Scanner(System.in).nextLine();
        String[] keys = s.split("\\s+");//   \s 表示空白字符   + 表示一到多个
        for (String key : keys) {
            c.queueBind(queue, "direct_logs", key);
        }

        // 回调对象
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            String s1 = new String(message.getBody());
            System.out.println("收到："+s1);
        };
        CancelCallback cancelCallback = consumerTag -> {};

        // 消费消息
        c.basicConsume(queue, true, deliverCallback, cancelCallback);
    }
}
