package m3;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * 发布和订阅模式
 *
 *      生产者不再声明队列, 而是声明 交换机, 使用交换机进行广播 消息... 若没有消息消费者,则消息 丢失 [就像 UDP传输信息]
 *      交换机只管发送队列信息, 不存储队列,
 *      交换机类型:
 *          Direct  直连
 *          Fanout  扇形
 *          Topic   主题
 *          Headers [很少使用,可忽略]
 *
 *
 */
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        // 创建 Fanout 类型交换机： 起名为 logs
        // c.exchangeDeclare("logs", "fanout", false, false, null);
        c.exchangeDeclare("logs", BuiltinExchangeType.FANOUT/*指明交换机类型*/);

        // 循环数据消息，发送
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();

            /*对 fanout 交换机，第二个参数指的是 消息队列的名字,
            这里由于交换机是群发消息,不需要指明消息队列的名字,指明也没有意义*/
            c.basicPublish("logs", "", null, s.getBytes());
        }
    }
}
