package m3;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * 发布和订阅模式
 *
 *      消费者声明 交换机和队列, 并将其绑定
 *
 */
public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        String queue = UUID.randomUUID().toString();
        // 1.创建随机队列
        c.queueDeclare(queue,false,true,true,null); //非持久，独占，自动删除
        // 2.创建交换机
        c.exchangeDeclare("logs", BuiltinExchangeType.FANOUT);
        // 3.将随机队列和交换机 绑定
        c.queueBind(queue, "logs", ""); //第三个参数对 fanout 交换机无效

        // 回调对象
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            String s = new String(message.getBody());
            System.out.println("收到："+s);
        };
        CancelCallback cancelCallback = consumerTag -> {};

        // 消费消息
        c.basicConsume(queue, true, deliverCallback, cancelCallback);

    }
}
