package m2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        /*
        创建队列,其中各个参数的含义：
           1.队列名
           2.是否是持久队列(如果不是持久队列,消息仅保存于内存中,服务停掉则 消息丢失, 持久队列,消息会持久到磁盘中)
           [当消息消费者 处理消息时都会出现阻塞的情况时,可以考虑将 消息队列设置为持久队列,避免消息丢失]
           3.是否是排他队列、独占队列 (就是某队列只允许某一个消费者接收)
           4.是否自动删除
           5.队列的其他参数属性
         */
        c.queueDeclare("task_queue-tyhgbevdwe", true,false,false,null);

        // 循环数据消息，发送
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            /*
            参数：
               1. 交换机(rabbitmq界面的exchange)，空串是默认交换机(就是界面上的第一个)
               3. 消息的属性数据
             */
            c.basicPublish("", "task_queue-tyhgbevdwe",
                    MessageProperties.PERSISTENT_BASIC, s.getBytes());
        }
    }
}
