package m1;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 简单模式
 *
 * 消息队列 消费者
 *
 *      消息队列的生产者和消费者都需要去 连接服务,创建(同一个)队列
 *      [自我理解: 不管是 哪一方先创建的消息队列, 首先缓存到 mq服务中,然后另一方便不会创建同一个队列...?]
 *
 */
public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException {

        // 创建连接工厂 对象
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection();
        Channel c = con.createChannel(); //通信通道

        // 创建队列
        c.queueDeclare("helloworld",false,false,false,null);

        // 处理消息的 回调对象 [里面封装了对消息 的处理操作]
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            byte[] body = message.getBody();
            String s = new String(body);
            System.out.println("收到："+s);
        };

        //取消处理消息的 回调对象 [当发现消息无法处理时执行]
        CancelCallback cancelCallback = consumerTag -> {};

        // 开始接收消息，消费消息，收到的消息会 交给 回调对象处理
        // c.basicConsume("helloworld", true, 处理消息的回调对象, 取消处理消息的回调对象);
        c.basicConsume("helloworld", true, deliverCallback, cancelCallback);
    }
}
