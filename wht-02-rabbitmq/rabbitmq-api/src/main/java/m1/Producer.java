package m1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消息队列 生产者
 *
 *      不管是消息 生产者还是消费者,都需要 声明队列, 这样不管谁先启动,
 *      都保证会有这个队列存在,而后启动的一方并不会创建同样的队列
 *
 */
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 创建连接工厂 对象
        ConnectionFactory f = new ConnectionFactory();
        // 配置要连接的 rabbitmq服务端
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");

        //获取连接对象
        Connection con = f.newConnection();
        //创建通道 对象
        Channel c = con.createChannel();

        // 在服务器上创建队列 helloworld
        // 如果队列已经存在，不会重复创建******************
        c.queueDeclare("helloworld",false,false,false,null);

        // 向 helloworld 队列发送消息
        c.basicPublish("", "helloworld", null, "Hello world!".getBytes());
        System.out.println("消息已发送");
    }
}
