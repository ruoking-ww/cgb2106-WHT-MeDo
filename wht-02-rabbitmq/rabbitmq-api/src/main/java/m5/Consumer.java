package m5;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * 主题模式
 *      实际上就是路由模式, 使用 的交换机类型为 topic
 *          特殊格式关键词
 *
 */
public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        // f.setVirtualHost("vh0");
        Channel c = f.newConnection().createChannel();

        // 1.随机队列 2.交换机 3.使用绑定键绑定
        // c.queueDeclare(queue, false,true,true,null);
        // 由服务器来自动随机命名,非持久,独占,自动删除
        String queue = c.queueDeclare().getQueue();
        c.exchangeDeclare("topic_logs", BuiltinExchangeType.TOPIC);
        System.out.println("输入绑定键，用空格隔开："); // "aa   bb   cc"
        String s = new Scanner(System.in).nextLine();
        String[] a = s.split("\\s+");//   \s空白字符   +一到多个
        for (String k : a) {
            c.queueBind(queue, "topic_logs", k);
        }

        // 回调对象
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            String s1 = new String(message.getBody());
            System.out.println("收到："+s1);
        };
        CancelCallback cancelCallback = consumerTag -> {};

        // 消费消息
        c.basicConsume(queue, true, deliverCallback, cancelCallback);
    }
}
