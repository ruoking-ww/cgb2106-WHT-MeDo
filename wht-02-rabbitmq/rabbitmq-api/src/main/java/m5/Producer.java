package m5;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * 主题模式
 *      实际上就是路由模式, 使用 的交换机类型为 topic
 *
 *  发布消息时可以使用 特殊格式关键词
 *
 */
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        //f.setVirtualHost("vh0");
        Channel c = f.newConnection().createChannel();

        // 创建 direct 类型交换机： direct_logs
        c.exchangeDeclare("topic_logs", BuiltinExchangeType.TOPIC);

        // 向 direct_logs 交换机发送消息，携带路由键关键词
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            System.out.print("输入路由键：");
            String k = new Scanner(System.in).nextLine();
            // 第二个参数是路由键
            // 对默认交换机，路由键就是队列名
            c.basicPublish("topic_logs", k, null, s.getBytes());
        }
    }
}
