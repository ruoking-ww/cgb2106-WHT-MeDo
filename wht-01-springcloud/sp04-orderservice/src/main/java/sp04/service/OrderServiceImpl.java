package sp04.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ruoking.sp01.pojo.Item;
import ruoking.sp01.pojo.Order;
import ruoking.sp01.pojo.User;
import ruoking.sp01.service.OrderService;
import ruoking.sp01.util.JsonResult;
import sp04.feign.ItemClient;
import sp04.feign.UserClient;

import java.util.List;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ItemClient itemClient;
    @Autowired
    private UserClient userClient;

    @Override
    public Order getOrder(String orderId) {

        log.info("获取订单，orderId="+orderId);

        // 远程调用商品，获取商品列表
        JsonResult<List<Item>> items = itemClient.getItems(orderId);
        // 远程调用用户，获取用户数据
        JsonResult<User> user = userClient.getUser(8);// 真实项目中要获取已登录用户的id

        Order order = new Order();
        order.setId(orderId);
        order.setUser(user.getData());
        order.setItems(items.getData());
        return order;
    }

    @Override
    public void addOrder(Order order) {

        log.info("添加订单："+order);

        // 远程调用商品，减少库存
        itemClient.decreaseNumber(order.getItems());
        // 远程调用用户，增加积分
        userClient.addScore(order.getUser().getId(), 1000);
    }
}
