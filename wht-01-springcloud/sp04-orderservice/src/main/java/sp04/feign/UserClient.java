package sp04.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ruoking.sp01.pojo.User;
import ruoking.sp01.util.JsonResult;

@FeignClient(name="user-service")
public interface UserClient {
    @GetMapping("/{userId}")
    JsonResult<User> getUser(@PathVariable Integer userId);

    @GetMapping("/{userId}/score")  //  /8/score?score=1000
    JsonResult<?> addScore(
            @PathVariable Integer userId,
            @RequestParam("score") Integer score);
}
